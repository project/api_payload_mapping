# External API payload mapping

- **Project site**: https://www.drupal.org/project/api_payload_mapping
- **Code**: https://www.drupal.org/project/api_payload_mapping/git-instructions
- **Issues**: https://www.drupal.org/project/issues/api_payload_mapping


## What Is This?

This module is intended to provide the payload mapping to the respective entities. It will give you the JSON
format like key value parameters of the mapped fields. Those JSON can be passed to an external API for their requirements.


## How To Install The Modules

1. The External API payload mapping project installs like any other Drupal module. There is
   extensive documentation on how to do this on
   [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules).
2. Within Drupal, enable External API payload mapping module in Admin
   menu > Extend.

If you find a problem, incorrect comment, obsolete or improper code or such,
please search for an issue about it in the
[issue queue](https://www.drupal.org/project/issues/api_payload_mapping). If there isn't
already an issue for it, please create a new one.

Thanks.