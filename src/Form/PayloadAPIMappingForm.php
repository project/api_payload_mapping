<?php

namespace Drupal\api_payload_mapping\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * API Payload mapping.
 *
 * @package Drupal\api_payload_mapping\Form
 */
class PayloadAPIMappingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'api_payload_mapping';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'api_payload_mapping.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('api_payload_mapping.settings');
    $num = $form_state->get('num_items');
    $new_payload_values = $form_state->get('payload');
    $removed_fields = $form_state->get('removed_fields');
    $wrapper_id = 'drupal-new-fieldset-wrapper';
    $form['#tree'] = TRUE;

    if ($num === NULL) {
      $num = $config->get('payload') ? count($config->get('payload')) : 1;
      $form_state->set('num_items', $num);
    }
    if ($new_payload_values === NULL) {
      $new_payload_values = $config->get('payload');
      $form_state->set('payload', $config->get('payload'));
    }
    // If no fields have been removed yet we use an empty array.
    if ($removed_fields === NULL) {
      $removed_fields = [];
      $form_state->set('removed_fields', $removed_fields);
    }
    $form['wrapper'] = [
      '#type' => 'details',
      '#title' => $this->t('API payload mapping'),
      '#open' => TRUE,
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
    ];
    $form['wrapper']['payload'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('API Key'),
        $this->t('Drupal Field'),
      ],
    ];

    for ($i = 0; $i < $num; $i++) {
      // Check if field was removed.
      if (in_array($i, $removed_fields)) {
        // Skip if field was removed and move to the next field.
        continue;
      }
      $form['wrapper']['payload'][$i] = [
        'key' => [
          '#type' => 'textfield',
          '#default_value' => isset($new_payload_values[$i]['key']) ? $new_payload_values[$i]['key'] : NULL,
        ],
        'field_name' => [
          '#type' => 'textfield',
          '#default_value' => isset($new_payload_values[$i]['field_name']) ? $new_payload_values[$i]['field_name'] : NULL,
        ],
        'actions' => [
          '#type' => 'submit',
          '#value' => $this->t('Remove'),
          '#name' => $i,
          '#submit' => ['::removeCallback'],
          '#ajax' => [
            'callback' => '::updateCallback',
            'wrapper' => $wrapper_id,
          ],
        ],
      ];
    }
    $form['wrapper']['help_text'] = [
      '#type' => 'help',
      '#markup' => t('In case of nested keys, use the format as key1:key2:key3 and for a term lookup, use the format as sourceTermField:vocabularyName:termFieldName.'),
    ];
    $form['wrapper']['actions'] = [
      '#type' => 'actions',
    ];
    $form['wrapper']['actions']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::updateCallback',
        'wrapper' => $wrapper_id,
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $new_payload_values = !empty($form_state->getValue('wrapper')['payload']) ? array_values($form_state->getValue('wrapper')['payload']) : [];
    $new_payload_array = [];
    foreach ($new_payload_values as $key => $value) {
      if ($value['key'] != '') {
        $new_payload_array[] = [
          'key' => $value['key'],
          'field_name' => $value['field_name'],
        ];
      }
    }
    $this
      ->config('api_payload_mapping.settings')
      ->set('payload', $new_payload_array)
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Callback for both ajax-enabled buttons.
   */
  public function updateCallback(array &$form, FormStateInterface $form_state) {
    return $form['wrapper'];
  }

  /**
   * Submit handler for the "add one" button.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $form_state
      ->set('num_items', $form_state->get('num_items') + 1)
      ->setRebuild();
  }

  /**
   * Submit handler for the "remove" button.
   *
   * Removes the corresponding line.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    /*
    * We use the name of the remove button to find
    * the element we want to remove
    * Line 88: '#name' => $i,.
    */
    $trigger = $form_state->getTriggeringElement();
    $indexToRemove = $trigger['#name'];

    // Remove the fieldset from $form (the easy way)
    unset($form['wrapper']['payload'][$indexToRemove]);

    // Remove the fieldset from $form_state (the hard way)
    // First fetch the fieldset, then edit it, then set it again
    // Form API does not allow us to directly edit the field.
    $namesFieldset = $form_state->getValue('wrapper')['payload'];
    unset($namesFieldset[$indexToRemove]);
    // $form_state->unsetValue('names_fieldset');
    $form_state->setValue('wrapper', $namesFieldset);

    // Keep track of removed fields so we can add new fields at the bottom
    // Without this they would be added where a value was removed.
    $removed_fields = $form_state->get('removed_fields');
    $removed_fields[] = $indexToRemove;
    $form_state->set('removed_fields', $removed_fields);

    // Rebuild form_state.
    $form_state->setRebuild();
  }

}
