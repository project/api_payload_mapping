<?php

namespace Drupal\api_payload_mapping\Services;

use Drupal\Component\Serialization\Json;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Exception\ClientException;

/**
 * Connect to API and post data.
 */
class PayloadApi {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $httpClient;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * AccessToken Client Constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Guzzle http Client.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   ConfigFactory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(
    ClientInterface $http_client,
    ConfigFactory $configFactory,
    LoggerChannelFactoryInterface $logger_factory) {
    $this->httpClient = $http_client;
    $this->configFactory = $configFactory;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * Get the API configuration.
   *
   * @return string
   *   API Endpoint.
   */
  public function getApiEndpoint() {
    // Load the API configuration.
    $payloadApiSettings = $this->configFactory->get('api_payload_mapping.settings');
    $apiEndpoint = $payloadApiSettings->get('website_request');
    return $apiEndpoint;
  }

  /**
   * Post payload data to Infra API.
   */
  public function postPayloadInfraApi($payload, $token) {
    $apiEndpoint = $this->getApiEndpoint();
    $infraAPIResponse = '';
    if ($apiEndpoint != '') {
      try {
        $response = $this->httpClient->post($apiEndpoint, [
          'headers' => ['Content-Type' => 'application/json', 'token' => $token],
          'body' => json_encode($payload),
        ]);
        if (!empty($response)) {
          $code = $response->getStatusCode();
          if ($code === 200) {
            $infraAPIResponse = Json::decode($response->getBody()->getContents());
            $infraAPIResponse['status'] = TRUE;
          }
        }
      }
      catch (ClientException $e) {
        // Catch the Client Exceptions.
        $this->loggerFactory->get('api_payload_mapping')->error('Client Exception: ' . $e->getMessage());
        $infraAPIResponse = [
          'status' => FALSE,
          'errorMessage' => $e->getMessage(),
        ];
      }
      catch (\Exception $e) {
        // Catch any other Exceptions.
        $this->loggerFactory->get('api_payload_mapping')->error('Exception: ' . $e->getMessage());
        $infraAPIResponse = [
          'status' => FALSE,
          'errorMessage' => $e->getMessage(),
        ];
      }
    }
    else {
      // Infra API endpoint not added.
      $this->loggerFactory->get('api_payload_mapping')->error('API endpoint is not added.');
      $infraAPIResponse = [
        'status' => FALSE,
        'errorMessage' => 'Infra API endpoint not added.',
      ];
    }

    return $infraAPIResponse;
  }

}
